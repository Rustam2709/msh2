﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Msh2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView1.RowHeadersVisible = false;
            dataGridView1.ColumnHeadersVisible = false;
            dataGridView2.RowHeadersVisible = false;
            dataGridView2.ColumnHeadersVisible = false;
            dataGridView3.RowHeadersVisible = false;
            dataGridView3.ColumnHeadersVisible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string mes = textBox1.Text;
            char[] m = mes.ToCharArray(); 
            string key = textBox2.Text; 
            string a = "абвгдежзийклмнопрстуфхцчшщъыьэюя";
            char[] alf = a.ToCharArray(); 
            char[] keykey = key.ToCharArray();

            dataGridView1.ColumnCount = 33;
            dataGridView1.RowCount = key.Length + 1;

            for (int i = 1; i <= a.Length; i++)
            {
                dataGridView1.Rows[0].Cells[i].Value = alf[i - 1];
            }

            for (int j = 1; j <= key.Length; j++)
            {
                dataGridView1.Rows[j].Cells[0].Value = keykey[j - 1];
            }

            //Заполнение 1ой таблицы
            for (int l = 0; l <= keykey.Length - 1; l++)
            {
                int index = Array.IndexOf(alf, keykey[l]);
                for (int k = 1; k <= a.Length; k++)
                {
                    if (index + k >= 33)
                    {
                        dataGridView1.Rows[l + 1].Cells[k].Value = alf[index + k - 33];
                    }
                    else dataGridView1.Rows[l + 1].Cells[k].Value = alf[index + k - 1];
                }
            }

            dataGridView2.RowCount = 3;
            dataGridView2.ColumnCount = mes.Length;

            for (int z = 0; z < mes.Length; z++)
            {
                dataGridView2.Rows[0].Cells[z].Value = m[z];
            }

            for (int ii = 0; ii < mes.Length; ii++)
            {
                int q = ii;
                if ((ii == keykey.Length) || (ii > keykey.Length))
                {
                    q = ii % keykey.Length;
                }
                dataGridView2.Rows[1].Cells[ii].Value = keykey[q];
            }

            for (int w = 0; w < m.Length; w++)
            {
                int ind = Array.IndexOf(alf, m[w]);
                int rw = w;
                if (w >= keykey.Length)
                {
                    rw = w % keykey.Length;
                }
                dataGridView2.Rows[2].Cells[w].Value = dataGridView1.Rows[rw + 1].Cells[ind + 1].Value;
            }

            for (int f = 0; f < m.Length; f++)
            {
                textBox3.Text = textBox3.Text + dataGridView2.Rows[2].Cells[f].Value;
                textBox4.Text = textBox3.Text;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string message = textBox4.Text; 
            char[] mm = message.ToCharArray();
            string key = textBox5.Text;
            string a = "абвгдежзийклмнопрстуфхцчшщъыьэюя";
            char[] alv = a.ToCharArray();
            char[] dkey = key.ToCharArray();

            dataGridView3.RowCount = 3;
            dataGridView3.ColumnCount = message.Length;

            for (int z = 0; z < message.Length; z++)
            {
                dataGridView3.Rows[0].Cells[z].Value = mm[z];
            }

            for (int ii = 0; ii < message.Length; ii++)
            {
                int q = ii;
                if ((ii == dkey.Length) || (ii > dkey.Length))
                {
                    q = ii % dkey.Length;
                }
                dataGridView3.Rows[1].Cells[ii].Value = dkey[q];
            }

            for (int b = 0; b < mm.Length; b++)
            {
                int gg = b;
                if (b >= dkey.Length)
                {
                    gg = b % dkey.Length;
                }
                string str = Convert.ToString(mm[b]);
                for (int f = 1; f < alv.Length; f++)
                {
                    if (str == Convert.ToString(dataGridView1.Rows[gg + 1].Cells[f].Value))
                    {
                        dataGridView3.Rows[2].Cells[b].Value = dataGridView1.Rows[0].Cells[f].Value;
                        break;
                    }
                }
                textBox6.Text = textBox6.Text + Convert.ToString(dataGridView3.Rows[2].Cells[b].Value); // Вывод в textBox дешифрованного сообщения
            }
        }
        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            //textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            //textBox5.Clear();
            textBox6.Clear();
        }
    }
}
